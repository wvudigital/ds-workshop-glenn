WVU Design System Workshop - Adam Glenn
==================

**Description:** WVU Design System Workshop - Adam Glenn.

**Developers name(s):** Adam Glenn

**Github repository URL:** [https://bitbucket.org/wvudigital/ds-workshop-glenn/src](https://bitbucket.org/wvudigital/ds-workshop-glenn/src)

**Documentation website URL:** [https://designsystem.sandbox.wvu.edu](https://designsystem.sandbox.wvu.edu)

**Dependencies necessary to work with this theme:** Sass.

## WVU Design System & Gulp

**Requirements**
* [NodeJS](https://nodejs.org)

You will need to install Node ^8.9.4.

  1. Download and install NodeJS from https://nodejs.org/en if you haven't already.
  1. Install Gulp globally by entering `npm install -g gulp` in your terminal.
  1. Navigate to your project's directory via terminal (something like `cd ~/Sites/cleanslate_themes/MY-SITE`)
  1. Install node modules by typing `npm ci`
  1. Run Gulp by typing `gulp`.

**Note:** the `gulpfile.js` in its base form will only compile your Sass.

Go to the (WVU Design System documentation website)[https://designsystem.wvu.edu] for more info.
